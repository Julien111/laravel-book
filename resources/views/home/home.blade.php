@extends('layouts/base')

@section('link')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/home/home.css') }}" >
@endsection

@section('content')

<div class="home">

    @if($bookElts->count())
    
    @foreach($bookElts as $book)


    <div class="shadow-md rounded-md overflow-hidden mx-auto" style="width: 350px;">

          <img src="{{ Storage::url($book->image->path) }}" class="img" alt="image de couverture du livre">

          <div class="p-4">
              <h5 class="text-xl font-semibold mb-2">{{ $book->title }}</h5>

              <p class="mb-4">{{ $book->resume }}</p>

              <a
                  href='{{ route("book_detail", $book->id) }}'
                  class="bg-purple-500 text-white active:bg-purple-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                  type="button">
                  More details
              </a>
          </div>
    </div>
    @endforeach  
    @else  
    <div class="flex flex-col justify-center">
        <div class="bg-yellow-300 bg-opacity-25 ">
            <p class="text-black">Aucune livre n'est pour l'instant affiché.</p>
        </div>
    </div>  
    @endif
</div>

@endsection