@extends('layouts/base')

@section('content')

<h1 class="text-gray-800 text-5xl text-red-800 text-center my-2">Contact</h1>
<div class="flex justify-center mt-3">
    <div class="shadow-md rounded-md overflow-hidden carte" style="width: 350px;">

        <div class="p-4">
            <h5 class="text-xl font-semibold mb-2">Lorem ipsum</h5>

            <p class="mb-4">Téléphone : XX XXX XX</p>
            <p class="mb-4">Email : lorem@ipsum.com</p>

        </div>
    </div>
</div>

@endsection