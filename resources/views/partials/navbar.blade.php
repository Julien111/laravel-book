<nav class="bg-blue-700 shadow dark:bg-blue-800">
    <div
      class="container px-6 py-3 mx-auto md:flex md:justify-between md:items-center"
    >
      <div class="flex items-center justify-between">
        <div>
          <a
            class="p-1 text-xl font-bold text-gray-200 dark:text-gray-200 md:text-2xl hover:text-yellow00 dark:hover:text-gray-50"
            href="#"
            >Laravel books</a
          >
        </div>        
      </div>

      <!-- Mobile Menu open: "block", Menu closed: "hidden" -->
      <div class="items-center md:flex">
        <div class="flex flex-col md:flex-row md:mx-6">
          <a
            class="my-1 p-1 text-gray-50 dark:text-gray-200 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('home') }}"
            >Accueil</a
          >
          @foreach($themeElts as $theme)
          <a
            class="my-1 p-1 text-gray-50 dark:text-gray-200 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('theme', $theme->name) }}"
            >{{ $theme->name }}</a>
          @endforeach
          <a
            class="my-1 p-1 text-gray-50 dark:text-gray-200 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('about') }}"
            >About</a>
          <a
            class="my-1 p-1 text-gray-50 dark:text-gray-200 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('contact') }}"
            >Contact</a>
            @guest
            <a
            class="bg-gray-50 p-1 rounded my-1 text-green-500 dark:text-gray-200 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('login') }}"
            >Connexion</a>
            <a
            class="bg-gray-50 p-1 rounded my-1 text-red-500 dark:text-yellow-400 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('register') }}"
            >Inscription</a>
            @endguest
            @auth 
            <a
            class="bg-green-700 p-1 rounded my-1 text-gray-50 dark:text-yellow-400 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('dashboard') }}"
            >Dashboard</a>
            @endauth
        </div>

     
      </div>
    </div>
  </nav>