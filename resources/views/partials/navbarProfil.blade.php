<nav class="bg-red-800 shadow dark:bg-red-800 mb-8">
    <div
      class="container px-6 py-3 mx-auto md:flex md:justify-between md:items-center"
    >
      <div class="flex items-center justify-between">
        <div>
          <a
            class="p-1 text-xl font-bold text-gray-200 dark:text-gray-200 md:text-2xl hover:text-yellow00 dark:hover:text-gray-50"
            href="#"
            >Laravel books profil</a
          >
        </div>        
      </div>

      <!-- Mobile Menu open: "block", Menu closed: "hidden" -->
      <div class="items-center md:flex">
        <div class="flex flex-col md:flex-row md:mx-6">
          <a
            class="my-1 p-1 text-gray-50 dark:text-gray-200 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('home') }}"
            >Accueil</a>  
          <a
            class="my-1 p-1 text-gray-50 dark:text-gray-200 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('dashboard') }}"
            >Dashboard</a>        
           
          <a class="my-1 p-1 text-gray-50 dark:text-gray-200 hover:text-yellow-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
            href="{{ route('profil.create') }}"
            >Ajouter un livre</a>

           <form method="POST" action="{{ route('logout') }}">
              @csrf
              <button class="bg-yellow-700 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" type="submit">Déconnexion</button>
            </form>
           
        </div>

     
      </div>
    </div>
  </nav>