@extends('layouts/profil')

@section('content')

<div class="flex flex-col items-center justify-center mt-12">
@if ($errors->any())
    <div role="alert my-8">
		<div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
			Erreur
		</div>

        <ul  class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">   
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

</div>

    <h1 class="text-gray-800 text-5xl text-red-800 text-center my-1">Formulaire ajout de livre</h1>

    <div class="flex flex-col items-center justify-center text-gray-700 my-8">
    	<form class="flex flex-col bg-white rounded shadow-lg p-12 mt-12" method="post" action="{{ route('profil.store') }}" enctype="multipart/form-data">
    		@csrf
            <label class="font-semibold text-xs" for="title">Titre du livre</label>
    		<input name="title" class="flex items-center h-12 px-4 w-64 bg-gray-200 mt-2 rounded focus:outline-none focus:ring-2" id='title' type="text">
    		<label class="font-semibold text-xs mt-3" for="resume">Résumé du livre</label>
    		<textarea id="resume" name="resume" class="flex items-center h-52 px-4 w-64 bg-gray-200 mt-2 rounded focus:outline-none focus:ring-2" width="200px"></textarea>
    		<label class="font-semibold text-xs" for="author">Auteur du livre</label>
    		<input name="author" class="flex items-center h-12 px-4 w-64 bg-gray-200 mt-2 rounded focus:outline-none focus:ring-2" id='author' type="text">
        
    		<label class="font-semibold text-xs mt-3" for="themes">Choisir le thème du livre</label>		
    		<select name="theme_id" class="form-select block w-full mt-1">
    			@foreach($themes as $theme)
    				<option value="{{ $theme->id }}">{{ $theme->name }}</option>
    			@endforeach
    		</select>

            <label class="font-semibold text-xs mt-3" for="opinion">Avis sur le livre :</label>
    		<textarea id="opinion" name="opinion" class="flex items-center h-52 px-4 w-64 bg-gray-200 mt-2 rounded focus:outline-none focus:ring-2" width="200px"></textarea>
        
    		<label class="font-semibold text-xs mt-3" for="imgFile">Choisir l'image de couverture :</label>

    		<input class="mt-2" type="file"
    		       id="imgFile" name="imgFile"
    		       accept="image/png, image/jpeg, image/jpg">
            <input id="user_id" name="user_id" type="hidden" value="{{Auth::id()}}">

            <button type="submit" class="flex items-center justify-center h-12 px-6 w-64 bg-blue-600 mt-8 rounded font-semibold text-sm text-blue-100 hover:bg-blue-700">Ajouter</button>
        
    	</form>

    </div>


@endsection