@extends('layouts/profil')

@section('content')

@if(session()->get('success'))
    <div class="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-3 my-3" role="alert">
      <p class="font-bold">{{ session()->get('success') }}</p>
    </div>
@endif


<div class="profil">

  <h1 class="text-gray-800 text-5xl text-red-800 text-center my-4">Votre liste de livres</h1>
  <div class="lg:grid lg:grid-cols-3 md:grid md:grid-cols-2 gap-4 sm:grid sm:grid-cols-1 gap-2">

    @if($yourBooks->count())
    
    @foreach($yourBooks as $book)

    <div class="shadow-md rounded-md overflow-hidden mx-auto mb-2" style="width: 350px;">

          <img src="{{ Storage::url($book->image->path) }}" class="img" alt="image de couverture du livre">

          <div class="p-4">
              <h5 class="text-xl font-semibold mb-2">{{ $book->title }}</h5>

              <p class="mb-4">{{ $book->resume }}</p>

              <a
                  href='{{ route("profil.show", $book->id) }}'
                  class="bg-purple-500 text-white active:bg-purple-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                  type="button">
                  More details
              </a>
          </div>
    </div>
    
    @endforeach  
    @else  
    <div class="flex flex-col justify-center">
        <div class="bg-yellow-300 bg-opacity-25 ">
            <p class="text-black">Vous n'avez aucun livre diffusé sur le site.</p>
        </div>
    </div>  
    @endif
  </div>
</div>

@endsection


