<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <form method="GET" action="{{ route('profil.create') }}">
              @csrf
              <button class="bg-blue-700 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" type="submit">Ajouter un livre</button>
            </form>
           
            <form method="GET" action="{{ route('profil.space') }}">
              @csrf
              <button class="bg-green-700 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-8" type="submit">Votre liste de livres</button>
            </form>
           
           <form method="POST" action="{{ route('logout') }}">
              @csrf
              <button class="bg-red-700 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" type="submit">Déconnexion</button>
            </form>
        </div>
    </div>
</x-app-layout>
