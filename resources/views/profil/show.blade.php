@extends('layouts/profil')

@section('content')

<div class="flex flex-col items-center justify-center mt-12">
@if ($errors->any())
    <div role="alert my-8">
		<div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
			Erreur
		</div>

        <ul  class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">   
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="book_detail my-8">

    <div class="shadow-md rounded-md overflow-hidden mx-auto" style="width: 350px;">

          <img src="{{ Storage::url($book->image->path) }}" class="img" alt="image de couverture du livre">

          <div class="p-4">
              <h5 class="text-xl font-semibold mb-2">{{ $book->title }}</h5>

              <p class="mb-4">{{ $book->resume }}</p>

              <div class="py-3 px-5 bg-gray-100 my-4">
                <p><b>Auteur</b> : {{ $book->author }}</p>
              </div>

              <h5 class="text-xl font-semibold mb-2">Avis sur le livre</h5>

              <p class="mb-4">{{ $book->opinion }}</p>     
              
              <p class="mb-4 text-red-800 text-lg">Thème du livre : {{ $book->theme->name }}.</p>            
              <p class="mb-4 text-green-700 text-lg">Date de publication : {{ $book->created_at->format('d-m-Y') }}.</p>            

              <div class="flex flex-row">
                <div>
                    <a
                        href='{{ route("profil.edit", ['id' => $book->id]) }}'
                        class="bg-yellow-500 text-white active:bg-purple-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                        type="button">
                        Modifier
                    </a>
                </div>
                <div>
                    <form action="{{ route('profil.destroy', $book->id)}}" method="post">
                        @csrf    
                        <button class="bg-red-400 hover:bg-red-700 text-white font-bold px-2 uppercase text-xs py-2 px-4 mr-1 border rounded" type="submit">Supprimer</button>
                    </form>
                </div>
                <div>
                    <a
                        href='{{ route("profil.space") }}'
                        class="bg-purple-500 text-white active:bg-purple-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                        type="button">
                        Retour
                    </a>
                </div>
              </div>
          </div>
    </div>
    
</div>

@endsection
