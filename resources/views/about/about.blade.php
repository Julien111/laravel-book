@extends('layouts/base')

@section('content')

<h1 class="text-gray-800 text-5xl text-red-800 text-center my-2">About us</h1>
<div class="flex justify-center mt-3">
    <div class="shadow-md rounded-md overflow-hidden carte" style="width: 350px;">

        <div class="p-4">
            <h5 class="text-xl font-semibold mb-2">Lorem ipsum</h5>

            <p class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, perferendis eos libero molestias iusto dolorem placeat a ullam in sequi at suscipit repellat? Ipsam, distinctio similique recusandae officiis delectus autem.Quisquam blanditiis cumque rem quis dolorem quas qui eum? A dignissimos fugiat sunt aliquid maiores incidunt, voluptates laborum distinctio veritatis, officiis non ipsum libero. Aliquam asperiores ab veniam id quibusdam.</p>            
        </div>
    </div>
</div>

@endsection