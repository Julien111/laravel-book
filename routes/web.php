<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//page d'accueil
Route::get('/', [HomeController::class, 'index'])->name('home');

//page par theme
Route::get('/theme/{theme}', [HomeController::class, 'pageTheme'])->name('theme');

//page pour chaque livre en détail
Route::get('/book/{id}', [HomeController::class, 'book'])->name('book_detail');

//page par contact
Route::get('/contact', [HomeController::class, 'contact'])->name('contact');

//page about us
Route::get('/about', [HomeController::class, 'about'])->name('about');

Route::get('profil/dashboard', function () {
    return view('profil/dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

//routes de la page profil

Route::get('/profil/index', [ProfilController::class, 'index'])->name('profil.space')->middleware('auth');

Route::get('/profil/create', [ProfilController::class, 'create'])->name('profil.create')->middleware('auth');
Route::post('/profil/store', [ProfilController::class, 'store'])->name('profil.store')->middleware('auth');

//voir le livre en détail sur la partie profil du user

Route::get('/profil/show/{id}', [ProfilController::class, 'show'])->name('profil.show')->middleware('auth');

//Modifier un livre (les routes)

Route::get('/profil/edit/{id}', [ProfilController::class, 'edit'])->name('profil.edit')->middleware('auth');
Route::post('/profil/update/{id}', [ProfilController::class, 'update'])->name('profil.update')->middleware('auth');

//méthode supprimer un livre

Route::post('profil/destroy/{id}', [ProfilController::class, 'destroy'])->name('profil.destroy')->middleware('auth');