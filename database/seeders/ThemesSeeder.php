<?php

namespace Database\Seeders;

use App\Models\Themes;
use Illuminate\Database\Seeder;

class ThemesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $theme = new Themes();
        $theme->name = "Policier";
        $theme->created_at = now();
        $theme->save();

        $theme = new Themes();
        $theme->name = "Science-fiction";
        $theme->created_at = now();
        $theme->save();

        $theme = new Themes();
        $theme->name = "Fantastique";
        $theme->created_at = now();
        $theme->save();    
    }

}