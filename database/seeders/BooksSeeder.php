<?php

namespace Database\Seeders;

use App\Models\Books;
use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $book = new Books();
        $book->title = "Harry Potter à l'école des sorciers";
        $book->resume = "Harry Potter, un jeune orphelin, est élevé par son oncle et sa tante qui le détestent. Alors qu'il était haut comme trois pommes, ces derniers lui ont raconté que ses parents étaient morts dans un accident de voiture. Le jour de son onzième anniversaire, Harry reçoit la visite inattendue d'un homme gigantesque se nommant Rubeus Hagrid, et celui-ci lui révèle qu'il est en fait le fils de deux puissants magiciens et qu'il possède lui aussi d'extraordinaires pouvoirs.";
        $book->author = "JK Rowling";
        $book->created_at = now();
        $book->theme_id = 3;
        $book->user_id = 3;
        $book->opinion = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi, repellendus nemo architecto officia explicabo.";
        $book->save();

        $book = new Books();
        $book->title = "Harry Potter et la chambre des secrets";
        $book->resume = "Une rentrée fracassante en voiture volante, une étrange malédiction qui s'abat sur les élèves, cette deuxième année à l'école des sorciers ne s'annonce pas de tout repos ! Entre les cours de potions magiques, les matches de Quidditch et les combats de mauvais sorts, Harry et ses amis Ron et Hermione trouveront-t-ils le temps de percer le mystère de la Chambre des Secrets ?";
        $book->author = "JK Rowling";
        $book->created_at = now();
        $book->theme_id = 3;
        $book->user_id = 3;
        $book->opinion = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi, repellendus nemo architecto officia explicabo.";
        $book->save();

        $book = new Books();
        $book->title = "Eragon";
        $book->resume = "Jeune garçon de ferme, Eragon mène une vie simple, jusqu'au jour où il ramasse dans la forêt une étrange pierre bleue. Le garçon découvre qu'il s'agit d'un œuf et assiste bientôt à la naissance... d'un dragon !
        En décidant de l'élever, il devient Dragonnier, héritier d'une caste d'élite que le terrible roi Galbatorix veut éliminer. Eragon n'a que seize ans, mais le destin du royaume de l'Alagaësia est entre ses mains.";
        $book->author = "Christopher Paolini";
        $book->created_at = now();
        $book->theme_id = 3;
        $book->user_id = 3;
        $book->opinion = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi, repellendus nemo architecto officia explicabo.";
        $book->save();

        $book = new Books();
        $book->title = "Bilbo le Hobbit";
        $book->resume = "Bilbo, comme tous les hobbits, est un petit être paisible et sans histoire. Son quotidien est bouleversé un beau jour, lorsque Grandalf le magicien et treize nains barbus l'entraînent dans un voyage périlleux.";
        $book->author = "J.R.R Tolkien";
        $book->created_at = now();
        $book->theme_id = 3;
        $book->user_id = 3;
        $book->opinion = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi, repellendus nemo architecto officia explicabo.";
        $book->save();

        $book = new Books();
        $book->title = "L'assassin royal";
        $book->resume = "Au château de Castelcerf le roi Subtil Loinvoyant règne sur les Six Duchés ; il est aidé dans sa lourde tâche par son fils Chevalerie qui, comme son père et tous les nobles du royaume, porte le nom de la qualité que ses parents espéraient le voir développer. Ainsi le frère du Roi-servant s'appelle-t-il Vérité et leur demi-frère, né d'un second lit, Royal. Suite à une aventure restée inconnue de tous, Chevalerie donne à la lignée un nouveau descendant : un bâtard, dont la simple existence va bouleverser le fragile équilibre qu'avait établi le roi pour contrôler ses turbulents fils. Ce héros malgré lui, nommé Fitz, voit son avenir s'assombrir au fil du temps.";
        $book->author = "Robin Hobb";
        $book->created_at = now();
        $book->theme_id = 3;
        $book->user_id = 3;
        $book->opinion = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi, repellendus nemo architecto officia explicabo.";
        $book->save();
    }
}