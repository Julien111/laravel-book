<?php

namespace Database\Seeders;

use App\Models\Images;
use Illuminate\Database\Seeder;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = new Images();
        $images->path = "images/default.jpg";
        $images->created_at = now();
        $images->book_id = 1;
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->created_at = now();
        $images->book_id = 2;
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->created_at = now();
        $images->book_id = 3;
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->created_at = now();
        $images->book_id = 4;
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->created_at = now();
        $images->book_id = 5;
        $images->save();
    }
}