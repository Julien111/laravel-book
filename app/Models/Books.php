<?php

namespace App\Models;

use App\Models\Images;
use App\Models\Themes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Books extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'resume', 'author', 'theme_id', 'user_id', 'opinion'];

    /**
     * function qui lie le livre au theme
     *
     * @return void
     */
    public function theme()
    {
        return $this->belongsTo(Themes::class, 'theme_id');
    }

    /**
     * Lien entre le user et un livre
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(Users::class);
    }

    /**
     * Get the image associated with the book
     *
     * @return void
     */
    public function image()
    {
        return $this->hasOne(Images::class, 'book_id');
    }

    
}