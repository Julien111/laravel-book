<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Themes extends Model
{
    use HasFactory;

    /**
     * fonction qui va lier nos themes aux livres
     *
     * @return void
     */
    public function bookHasTheme ()
    {
    return $this->hasMany(Books::class);
    }
}