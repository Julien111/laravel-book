<?php

namespace App\Models;

use App\Models\Books;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Images extends Model
{
    use HasFactory;

    /**
     * Lien entre images et books (récupérer un livre avec l'image)
     *
     * @return void
     */
    public function book()
    {
        return $this->belongsTo(Books::class);
    }
}