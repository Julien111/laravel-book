<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Models\Images;
use App\Models\Themes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        if (Auth::check()) {
        $books = new Books;
        $yourBooks = $books->where('user_id', Auth::id())->get();        
        return view('profil/index', compact('yourBooks'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $themes = Themes::all();        
        return view('profil/create', compact('themes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validated = $request->validate([
                'title' => 'required|max:255|min:5|unique:books',
                'resume' => 'required',
                'author' => 'required|min:3',
                'theme_id' => 'required',
                'user_id' => 'required',
                'opinion' => 'required',
                'imgFile' => 'required',
        ]);

        $path = Storage::disk('public')->put('images', $request->imgFile);

        if($validated){

            $book = Books::create([
                'title' => $request->title,
                'resume' => $request->resume,
                'author' => $request->author,
                'theme_id' => $request->theme_id,
                'user_id' => $request->user_id,
                'opinion' => $request->opinion,
            ]);

            //On enregistre l' images en base de données

            $image = new Images;
            $image->path = $path;

            $book->image()->save($image);

            return redirect('/profil/index')->with('success', 'Le livre a bien été enregistré.');

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $books= new Books;
        $book = $books->findOrFail($id);
                
        return view('profil/show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Books::findOrFail($id);
        $themeId = $book->theme_id;
        $themes = Themes::all();

        return view('profil/edit', compact('themes', 'themeId','book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        if(empty($request->imgFile)){
            $validated = $request->validate([
                'title' => 'required|max:255|min:3',
                'resume' => 'required',
                'author' => 'required',
                'theme_id' => 'required',               
                'opinion' => 'required',               
            ]);

            Books::whereId($id)->update($validated);

            return redirect('/profil/index')->with('success', 'Vos données ont été mises à jour avec succès.');
        }

        if(!empty($request->imgFile)){

            $validated = $request->validate([
                'title' => 'required|max:255|min:3',
                'resume' => 'required',
                'author' => 'required',
                'theme_id' => 'required',
                'opinion' => 'required',               
            ]);

            if($validated && isset($request->imgFile)){    
                
                //retrouver l'image liée au livre et la supprimer
                $file = DB::table('images')->where('book_id', $id)->value('path');

                
                //suppression du fichier image 

                unlink(storage_path('app/public/'.$file));
                                
                //on enregistre et stock l'image en bdd   

                $path = Storage::disk('public')->put('images', $request->imgFile);
                DB::table('images')->where('book_id', $id)->update(['path' => $path]);

                Books::whereId($id)->update($validated);

                return redirect('/profil/index')->with('success', 'Vos données ont été mises à jour avec succès.');

            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //retrouver l'image liée au fichier et la supprimer 
        $file = DB::table('images')->where('book_id', $id)->value('path');
        
        //suppression du fichier image dans storage

        unlink(storage_path('app/public/'. $file));

        $book = Books::findOrFail($id);
        $book->delete();

        return redirect('/profil/index')->with('success', 'Le livre a bien été supprimé.');
    }
}