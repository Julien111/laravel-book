<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Models\Themes;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    /**
     * Fonction qui retourne la page d'accueil du site
     *
     * @return void
     */
    public function index ()
    {
        $themes = new Themes;
        $themeElts = $themes->all();
        $books = new Books;
        $bookElts = $books->orderByRaw('updated_at - created_at ASC')->limit(3)->get();
        
        return view('home/home', compact('themeElts', 'bookElts'));
    }

    /**
     * Page qui affiche des livres selon le theme
     *
     * @param [string] $theme
     * @return void
     */
    public function pageTheme (string $theme)
    {
        $themes = new Themes;
        $themeElts = $themes->all();

        $objTheme = DB::table('themes')->where('name', $theme)->first();
        
        $books= new Books;
        $bookElts = $books->where('theme_id', $objTheme->id)->get();        
                
        return view('theme/theme', compact('themeElts', 'bookElts', 'objTheme'));
    }

    /**
     * Fonction qui retourne la page contact
     *
     * @return void
     */
    public function contact ()
    {
        $themes = new Themes;
        $themeElts = $themes->all();
        
        return view('contact/contact', compact('themeElts'));
    }

    /**
     * Fonction qui retourne la page about us
     *
     * @return void
     */
    public function about ()
    {
        $themes = new Themes;
        $themeElts = $themes->all();
        
        return view('about/about', compact('themeElts'));
    }

    /**
     * Page qui affiche le livre en détail
     *
     * @param [int] $id
     * @return void
     */
    public function book (int $id)
    {
        $themes = new Themes;
        $themeElts = $themes->all();

        $books= new Books;
        $book = $books->findOrFail($id);
                
        return view('book/book', compact('themeElts', 'book'));
    }

}